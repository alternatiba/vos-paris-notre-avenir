Le projet est un peu jeté là comme ça, il n'y a pas eu particulièrement
d'efforts pour aider à l'adapter à une autre situation pour l'instant. Mais
vous pouvez me contacter pour qu'on en parle ;)

Mais globalement, il y a un composant par écran du site
[vos-paris-notre-avenir.org](https://vos-paris-notre-avenir.org), c'est
relativement simple à comprendre.

## Technos utilisées

- le cadriciel [Svelte](https://svelte.dev/)

- [Tailwind](https://tailwindcss.com/) pour le CSS, avec
  [PurgeCSS](https://purgecss.com/) (comme ça vous êtes prévenu·es)

## Utilisation

### Préparation de l'environnement

```sh
yarn install
```

### Développement

Pour lancer un serveur de développement, avec application à la volée des
modifications :

```sh
yarn start
```

### Déploiement

```sh
yarn build
```

Ensuite, le dossier `dist` peut être envoyé sur un serveur et tout roule.

### Trucs et astuces

- parfois `parcel`, qui est utilisé pour tout compiler, plante un peu, faut pas
  hésiter à supprimer le dossier `.cache` dans ce cas.

- c'est pas mal de vider `dist` de temps en temps, histoire de ne pas déployer
  trop de vieux fichiers

