import creditAgricole from '../assets/images/logos/Crédit_Agricole.svg'
import societeGenerale from '../assets/images/logos/Société_Générale.svg'
import bnp from '../assets/images/logos/BNP_Paribas.svg'
import bpce from '../assets/images/logos/bpce.png'
import rothschild from '../assets/images/logos/rothschild.png'
import blackrock from '../assets/images/logos/blackrock.svg'
import axa from '../assets/images/logos/AXA.svg'
import scor from '../assets/images/logos/scor.png'

import bonnafe from '../assets/images/pdg/bonnafe.png'
import brassac from '../assets/images/pdg/brassac.png'
import buberl from '../assets/images/pdg/buberl.png'
import fink from '../assets/images/pdg/fink.png'
import kessler from '../assets/images/pdg/kessler.png'
import mignon from '../assets/images/pdg/mignon.png'
import oudea from '../assets/images/pdg/oudea.png'
import pdgrothschild from '../assets/images/pdg/rothschild.png'

import bonnafeSmall from '../assets/images/pdg_small/bonnafe.jpg'
import brassacSmall from '../assets/images/pdg_small/brassac.jpg'
import buberlSmall from '../assets/images/pdg_small/buberl.jpg'
import finkSmall from '../assets/images/pdg_small/fink.jpg'
import kesslerSmall from '../assets/images/pdg_small/kessler.jpg'
import mignonSmall from '../assets/images/pdg_small/mignon.jpg'
import oudeaSmall from '../assets/images/pdg_small/oudea.jpg'
import pdgrothschildSmall from '../assets/images/pdg_small/rothschild.jpg'

const tweetBanques = `Les banques se rassemblent à huis clos pour leurs #AG2020. Avec + de 198 milliards d'€ de financements aux énergies fossiles depuis la #Cop21, interpellons leurs PDG déconnectés de l'urgence climatique 😱#VosParisNotreAvenir #PourLeJourDAprès
👉vos-paris-notre-avenir.org`

const rapportLink = 'amisdelaterre.org/communique-presse/krach-petrolier-banques-investisseurs-epingles-soutiens-petrole-gaz-de-schiste/'

export default [
  {
    nom: 'Crédit Agricole',
    logo: creditAgricole,
    pdg: {
      nom: 'Philippe Brassac',
      photo: brassac,
      photoSmall: brassacSmall,
    },
    killerFacts: [
      '42 milliards d’euros de financements aux <strong>énergies fossiles</strong> depuis l’Accord de Paris',
      '5,5 milliards d’euros de financements aux <strong>pétrole et gaz de schiste</strong> depuis l’Accord de Paris',
    ],
    tweets: [
      tweetBanques,
      `Bon, @PhilippeBrassac. Si on fait le point, @CreditAgricole  depuis la #Cop21 c'est 4278 milliards d’euros de financement aux énergies fossiles😲
Vous allez continuer longtemps à financer le chaos climatique ? #VosParisNotreAvenir #PourLeJourDAprès
👉vos-paris-notre-avenir.org`,
      `.@PhilippeBrassac, vous attendez quoi pour que @CreditAgricole cesse ses financements climaticides ? + 6 milliards $ aux pétrole et gaz de schiste depuis la #Cop21… STOP⛔️#VosParisNotreAvenir #PourLeJourDAprès
📙Conseil lecture : ${rapportLink}
👉vos-paris-notre-avenir.org`,
    ],
  },

  {
    nom: 'Société Générale',
    logo: societeGenerale,
    pdg: {
      nom: 'Frédéric Oudéa',
      photo: oudea,
      photoSmall: oudeaSmall,
    },
    killerFacts: [
      '50 milliards d’euros de financements aux <strong>énergies fossiles</strong> depuis l’Accord de Paris',
      '10 milliards d’euros de financements aux <strong>pétrole et gaz de schiste</strong> depuis l’Accord de Paris et joue actuellement un rôle clé dans le méga <strong>projet Rio Grande LNG</strong>',
    ],
    tweets: [
      tweetBanques,
      `Depuis la #Cop21, @SocieteGenerale c'est le #⃣1⃣ financeur 🇫🇷 aux pétrole et gaz de schiste, avec 10 milliards € 💸@FredericOudea : cesser vos paris nocifs sur notre avenir ! #VosParisNotreAvenir #PourLeJourDAprès
Conseil lecture ${rapportLink}
👉vos-paris-notre-avenir.org`,
      `.@FredericOudea : pour l'#AG2020 de @SocieteGenerale, ouvrez les yeux : vos financements aux projets d’énergies fossiles ont des conséquences dramatiques. Agissez ENFIN pour y mettre fin ! #VosParisNotreAvenir
Conseil lecture ${rapportLink}
👉vos-paris-notre-avenir.org`,
    ],
  },

  {
    nom: 'BNP Paribas',
    logo: bnp,
    pdg: {
      nom: 'Jean-Laurent Bonnafé',
      photo: bonnafe,
      photoSmall: bonnafeSmall,
    },
    killerFacts: [
      '78 milliards d’euros de financements aux <strong>énergies fossiles</strong> depuis l’Accord de Paris',
      '8 milliards d’euros de financements à l’expansion du <strong>charbon</strong> depuis 2017',
    ],
    tweets: [
      tweetBanques,
      `.@BNPParibas c'est €78 milliards de financements aux énergies fossiles depuis #Cop21 et un soutien ↗️ aux majors comme @Total. Ça promet le grand soir mais ça continue de creuser !
#VosParisNotreAvenir
📙Conseil lecture : ${rapportLink}
👉vos-paris-notre-avenir.org`,
      `.@BNPParibas a mal lu la notice : pour sortir du charbon, il faut arrêter de financer les entreprise, qu’elles polluent notre air avec leurs centrales ou creusent toujours dans leurs mines💸#VosParisNotreAvenir
📙Conseil lecture : ${rapportLink}
👉vos-paris-notre-avenir.org`,
    ],

  },

  {
    nom: 'Banque Populaire Caisse d’Épargne',
    logo: bpce,
    pdg: {
      nom: 'Laurent Mignon',
      photo: mignon,
      photoSmall: mignonSmall,
    },
    killerFacts: [
      '28 milliards d’euros de financements aux <strong>énergies fossiles</strong> depuis l’Accord de Paris',
      '3,1 milliards d’euros de financements aux <strong>pétrole et gaz de schiste</strong> depuis l’Accord de Paris',
    ],
    tweets: [
      tweetBanques,
      `Depuis la #Cop21, le @GroupeBPCE c'est 28 milliards d'€ de financements aux énergies fossiles. 💸
Qu'attendez-vous pour y mettre un terme définitivement ? #VosParisNotreAvenir #PourLeJourDAprès
📙Conseil lecture ${rapportLink}
👉vos-paris-notre-avenir.org`,
      `Depuis la #Cop21, @GroupeBPCE c'est 3 milliards € de financements aux pétrole et gaz de schiste😱
Les conséquences sont désastreuses alors que plus que jamais nous devons agir. #PourLeJourDAprès #VosParisNotreAvenir
📙À lire ${rapportLink}
👉vos-paris-notre-avenir.org`,
    ],
  },

  {
    nom: 'Rothschild & Co',
    logo: rothschild,
    pdg: {
      nom: 'Alexandre de Rothschild',
      photo: pdgrothschild,
      photoSmall: pdgrothschildSmall,
    },
    killerFacts: [
      '400 millions d’euros d’investissements dans l’expansion des <strong>pétrole et gaz de schiste</strong> en 2020',
      'Aide en douce les entreprises comme Engie à revendre leurs centrales à <strong>charbon</strong> au lieu de les fermer',

    ],
    tweets: [
      `Le financier Rothschild & Co c'est 400 millions d’€ d’investissements dans l’expansion des pétrole et gaz de schiste. Direction chaos climatique. #VosParisNotreAvenir
📙Conseil lecture ${rapportLink}
👉Interpellons Rothschild : vos-paris-notre-avenir.org`,
    ],
  },

  {
    nom: 'BlackRock',
    logo: blackrock,
    pdg: {
      nom: 'Larry Fink',
      photo: fink,
      photoSmall: finkSmall,
    },
    killerFacts: [
      '1er investisseur du monde dans les <strong>énergies fossiles</strong>, avec 80 milliards d’euros en 2019',
      '16 milliards d’euros d’investissements dans l’expansion du <strong>charbon</strong> en 2019',
    ],
    tweets: [
      `.@Blackrock : 1er investisseur du monde dans les énergies fossiles avec 80 milliards € en 2019. Il est temps de mettre un terme à cette finance déconnectée de l'urgence climatique ! #VosParisNotreAvenir
📙Conseil lecture ${rapportLink}
👉 vos-paris-notre-avenir.org`,
      `.@Blackrock : 16 milliards d’euros d’investissements dans l’expansion du charbon en 2019. Il est temps de mettre un terme à cette finance déconnectée de l'urgence climatique ! #VosParisNotreAvenir
📙Conseil lecture ${rapportLink}
👉 vos-paris-notre-avenir.org`,
    ],
  },

  {
    nom: 'AXA',
    logo: axa,
    pdg: {
      nom: 'Thomas Buberl',
      photo: buberl,
      photoSmall: buberlSmall,
    },
    killerFacts: [
      '900 millions d’euros d’investissements dans l’expansion du <strong>gaz et pétrole de schiste</strong> en 2020',
      '110 millions d’euros d’investissements dans <strong>Total</strong> à qui il ne fait aucune demande sur le climat',
    ],
    tweets: [
      `.@thomasbuberl, @AXAfrance c'est 900 millions € d’investissements dans l’expansion du gaz et pétrole de schiste en 2020 : mettez un terme cette finance déconnectée de l'urgence climatique #VosParisNotreAvenir
📙Conseil lecture ${rapportLink}
👉 vos-paris-notre-avenir.org`,
      `.@thomasbuberl, @AXAfrance c'est 110 millions € d’investissements dans @Total à qui il ne fait aucune demande sur le climat. Changez de cap ! #VosParisNotreAvenir
📙Conseil lecture ${rapportLink}
👉 vos-paris-notre-avenir.org`,
    ],
  },

  {
    nom: 'Scor',
    logo: scor,
    pdg: {
      nom: 'Denis Kessler',
      photo: kessler,
      photoSmall: kesslerSmall,
    },
    killerFacts: [
      '5ème réassureur du monde et qui n’a aucune restriction sur les <strong>pétrole et gaz</strong>',
      'Assure et réassure toujours des <strong>énergies fossiles</strong> dans lesquelles il refuse pourtant d’investir',

    ],
    tweets: [
      `Alors @SCOR_SE assure et réassure toujours des énergies fossiles dans lesquelles il refuse pourtant d’investir. Ironique non ? Changez de cap ! #VosParisNotreAvenir
📙Conseil lecture ${rapportLink}
👉 vos-paris-notre-avenir.org`,
      `.@SCOR_SE est le 5ème réassureur du monde et n’a aucune restriction sur le secteur des pétrole et gaz. Il serait temps d'agir ENFIN et mettre un terme à vos activités climaticides ! #VosParisNotreAvenir
📙Conseil lecture ${rapportLink}
👉 vos-paris-notre-avenir.org`,
    ],
  },
]
