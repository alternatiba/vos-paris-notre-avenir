import { writable } from 'svelte/store'

import fond1 from '../assets/images/fonds/190826-amazon-brazil-fire-cs-834a_eef05addd0d4dc77d766710fa90413d8.jpg'
import fond2 from '../assets/images/fonds/2500.jpg'
import fond3 from '../assets/images/fonds/40738505853_e47c977f63_o.jpg'
import fond4 from '../assets/images/fonds/40738508593_ef386c691a_o.jpg'
import fond5 from '../assets/images/fonds/arton2920.jpg'
import fond6 from '../assets/images/fonds/catastrophe pétrole 1.jpg'
import fond7 from '../assets/images/fonds/catastrophe pétrole 2.jpg'
import fond8 from '../assets/images/fonds/mega-drought-california-drought.jpg'
import fond9 from '../assets/images/fonds/mine charbon 2.jpg'
import fond10 from '../assets/images/fonds/mine charbon 3.jpg'
import fond11 from '../assets/images/fonds/mine charbon 4.jpg'
import fond12 from '../assets/images/fonds/mine charbon.jpg'
import fond13 from '../assets/images/fonds/petrole 2.jpg'
import fond14 from '../assets/images/fonds/petrole.jpg'
import fond15 from '../assets/images/fonds/photo_2020-05-01_15-46-20.jpg'

export const fonds = [ fond1, fond2, fond3, fond4, fond5, fond6, fond7, fond8, fond9, fond10, fond11, fond12, fond13, fond14, fond15 ]

export const randomFonds = writable(null)

export const selectedBank = writable(null)

export const selectedImage = writable(null)

export const selectedText = writable(null)

export const currentRoute = writable()
