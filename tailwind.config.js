module.exports = {
  purge: [
    './src/*.html',
    './src/**/*.svelte',
  ],
  theme: {
    extend: {
      rotate: {
        '-8': '-8deg',
      },

      colors: {
        'vert-at': '#00ae41',
        'transparent-white': '#fff8',
        'less-transparent-white': '#fffa',
      },
    },
    fontFamily:{
      'breul': 'breul',
      'gemeli': 'gemeli',
      'source-code-pro': 'source code pro',
    },
  },
  variants: {},
  plugins: [],
}
